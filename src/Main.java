import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        // Как я понял, текст вводится в одну строку.
        String text = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8)).readLine();

        // Stream, который преобразуем в словарь
        Stream<String> wordsStream = getStreamFromText(text);

        // Словарь: слово - количество этого слова в тексте
        HashMap<String, Integer> wordsCounter = new HashMap<>(wordsStream.collect(Collectors.toMap(s -> s, s -> 1, Integer::sum)));

        // Отсортированный стрим с уникальными значениями
        Stream<String> sortedWordsStream = getStreamFromText(text)
                .distinct()
                .sorted((s1, s2) -> compareByValue(wordsCounter, s1, s2));
        sortedWordsStream.limit(10).forEach(System.out::println);
    }

    public static Stream<String> getStreamFromText(String text) {
        // Stream слов, разбиваем полученную ранее строку по всем символам, что не являются буквой (на всякий случай сделал ещё и кириллической) или цифрой.
        return Arrays.stream(text.split("[^A-Za-z0-9\\p{IsCyrillic}]"))
                .filter(w -> !w.equals("")) // исключаем пустые строки
                .map(String::toLowerCase); // переводим в нижний регистр для удобного сравнения
    }
    public static int compareByValue(HashMap<String, Integer> map, String str1, String str2) {
        // Сортировка по убыванию
        if (map.get(str1) > map.get(str2))
            return -1;
        else if (map.get(str1) < map.get(str2))
            return 1;
        else
            return str1.compareTo(str2);
    }
}